import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import {MatTooltipModule} from '@angular/material/tooltip';
import { AppComponent } from './app.component';
import {MatButtonToggleModule, MatInputModule} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatAutocompleteModule,
  MatOptionModule,
  MatSelectModule,
  MatSlideToggleModule,
  MatIconModule,
  MatMenuModule,
  MatSortModule,
  MatTableModule,
  MatNativeDateModule,
  MatDatepickerModule,
  MatRadioModule
} from '@angular/material';
const material_module = [
  BrowserAnimationsModule,
  MatInputModule,
  MatButtonModule,
  MatCheckboxModule,
  MatAutocompleteModule,
  MatOptionModule,
  MatSelectModule,
  MatIconModule,
  MatMenuModule,
  MatSlideToggleModule,
  MatSortModule,
  MatTableModule,
  MatIconModule,
  MatMenuModule,
  MatSlideToggleModule,
  MatTableModule,
  MatNativeDateModule,
  MatDatepickerModule,
  MatRadioModule
];
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule, AngularFontAwesomeModule,
    HttpClientModule,
    MatInputModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatButtonToggleModule,
    MatTooltipModule,
    FormsModule,
    material_module
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  private static AppRoutingModule: any;
}
